# Ansible - Custom VPC setup in AWS & EC2 creation



## Yaml file

```
#Maintainer: GINU MATHEW
#VPC Creation

- name: Test creating ec2 instance with Ansible
  hosts: localhost
  connection: local
  vars: 
    cidr_block_vpc: 172.168.0.0/16
    region_name:  eu-west-1
    vpc_name: customvpc
    cidr_id_public_sub: 172.168.0.0/17
    keypair_name: ginu
    cidr_ip_port: 0.0.0.0/0
  tasks: 
    - name: "VPC creation"
      ec2_vpc_net:
        cidr_block: "{{ cidr_block_vpc }}"
        region: "{{ region_name }}"
        name: "{{ vpc_name }}"
      register: ginuvpc
    - name: "get vpc id"
      set_fact:
        vpc_id: "{{ ginuvpc.vpc.id }}"
#Public - Subnet 1
    - name: "Subnet -public-1"
      ec2_vpc_subnet:
        state: present
        vpc_id: "{{ vpc_id }}"
        region: "{{ region_name }}"
        cidr: "{{ cidr_id_public_sub }}"
        map_public: yes
        tags:
          Name:  "Public-subnet1"
      register: subnetpublic

    - name: "get subnet id"
      set_fact: 
        public_subnet: "{{ subnetpublic.subnet.id }}"
#Internet Gateway
    - name: "Internet Gateway"
      ec2_vpc_igw:
        vpc_id: "{{ vpc_id }}"
        state: present
        region: "{{ region_name }}"
        tags:
          Name: "IGW"
      register: Internetgw
    - name: "get IG id"
      set_fact: 
        igw_id: "{{ Internetgw.gateway_id }}"
#Route Table
    - name: "Route Table for public"
      ec2_vpc_route_table: 
        vpc_id: "{{ vpc_id }}"
        region: "{{ region_name }}"
        tags:
          Name: "RT_Public"
        subnets: 
          - "{{ public_subnet }}"
        routes: 
          - dest: "0.0.0.0/0"
            gateway_id: "{{ igw_id }}"
#Security Group for custom vpc
#Allow ports - 80,443 & 22
    - name: "Security groups for EC2"
      ec2_group:
        name: "SG1"
        description: EC2 security group
        vpc_id: "{{ vpc_id }}"
        region: "{{ region_name }}"
        rules:
          - proto: tcp
            ports:
            - 80
            cidr_ip: "{{ cidr_ip_port }}"
            rule_desc: allow all on port 80
          - proto: tcp
            ports:
            - 443
            cidr_ip: "{{ cidr_ip_port }}"
            rule_desc: allow all on port 443
          - proto: tcp
            ports:
            - 22
            cidr_ip: "{{ cidr_ip_port }}"
            rule_desc: allow all on port 22
        state: present
        tags:
          Name: "SG 1"
      register: sg1
#EC2 Creation from the custom VPC
    - name: aws ec2 creation
      ec2:
        key_name: "{{ keypair_name }}"
        instance_type: t2.micro
        image: ami-0aef57767f5404a3c
        region: "{{ region_name }}"
        wait: yes
        group_id: "{{ sg1.group_id }}"
        vpc_subnet_id: "{{ subnetpublic.subnet.id }}"
        assign_public_ip: yes






```


